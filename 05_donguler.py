## Döngüler
# For döngüleri
"""
For döngüleri, önceden belirtilmiş bir liste içerisindeki elemanları sırası ile döndürerek çalışır
For döngülerinin işlemesinde her turda değişen bir değişkenimiz vardır.
Bu değişken For döngüsünün döndürdüğü içerikteki sıradaki elemandır.
"""
ornek_liste = ['Berkay', 'Yargi', 'Oguzhan']

for ogrenci in ornek_liste:
    print(ogrenci, 'Derse Katildi')

# range komutu kendisine verdiğimiz iki sayı arasındaki sayılardan oluşan bir liste oluşturur. İlk eleman dahil, ikinci eleman dahil değil şekildedir.
for i in range(1, 51):
    i

# While döngüleri
"""
While döngüleri, for döngülerinin aksine belirli bir veri seti ile değil, bir koşul doğru olduğu süre içerisinde çalışır.
"""
i = 0

# Burada while döngüsü i değişkeni 10'dan küçük ya da 10'a eşit olduğu sürece devam edip, i 10'dan büyük olduğu noktada duracaktır
while i <= 10:
    print(i)
    i += 1

## Döngüleri kırma yöntemleri

# Continue - Bu komut döngü esnasında çalışırsa, döngünün o esnadaki turunu orada bitirip bir sonraki tura geçmeye zorlar

ornek_liste = ['ali', 'veli', 'hasan', 'huseyin']

for i in ornek_liste:
    if i == 'hasan':
        continue
    print(i)

# Break - Continue'ye benzer şekilde döngüyü kırar, ancak continue'nun aksine sonraki tura geçmez, döngüden direkt çıkılır

for i in ornek_liste:
    if i == 'hasan':
        break
    print(i)
print('Döngü kırıldı')

i = 0
while True:
    if i == 5:
        break
    print(i)
    i += 1