# En başta `range(1, 51)` ile 1'den 50'ye kadar bir döngü oluşturuyoruz
for i in range(1, 51):
    # Burada sayının hem 3'e hem de 5'e bölündüğünü kontrol ediyoruz.
    # 15 şeklinde yazmamız, en küçük ortak bölen 15 olduğu için sisteme daha az işlem yaptırmak adına
    # Bu kontrolün en başta yapılmasının sebebi, bundan geçecek her sayı, 3 ya da 5'e bölünmenin kontrolünden de
    # geçeceği için bu sorguya hiç girmeden diğerlerinden birinin çıktısını verecektir. 
    if i % 15 == 0:  # if i % 3 == 0 and i % 5 == 0:
        print('FizzBuzz')
    elif i % 5 == 0:
        print('Buzz')
    elif i % 3 == 0:
        print('Fizz')
    else:
        print(i)