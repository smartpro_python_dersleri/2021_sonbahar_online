* "Sen Abdülhamidi savunmuşsun! Hayır savunmadım! Savundun! Çıkar Göster!" örneğini kodunuzun en başında bir değişken olarak atayın.
* Bu stringde büyük-küçük harf önemlidir.
* Sonrasında sırasıyla bu metindeki bütün sesli harfleri ve "!" karakterlerini ayrı ayrı sayın.
* En son çıktıda "a = 5" şeklinde bütün sesli harfler ve ünlemlerin sayımlarını yazdırın.

Ek maddeler:
* Bunu sayılacak ögeler ve sayımın yapılacağı string ayrıca belirtilebilir şekilde bir fonksiyona çevirmeniz.
* Print yerine kod içinde kullanılabilecek bir dict olarak çıktı verebilmeniz. Bunu yaparken isteğe bağlı olarak print de hala yapılabilir olmalı.

Örnek çıktı:

```
a = 8
e = 6
ı = 4
....
...
..
```