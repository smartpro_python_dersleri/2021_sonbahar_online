* 1'den 50'ye kadar bir döngü dönecek
* eğer o turdaki sayı 3'e bölünüyorsa ekrana "Fizz" yazılacak
* eğer o turdaki sayı 5'e bölünüyorsa ekrana "Buzz" yazılacak
* Eğer o turdaki sayı hem 3'e hem de 5'e bölünüyorsa ekrana "FizzBuzz" yazılacak
* Diğer bütün koşullarda ise direkt sayı ekrana yazılacak

örnek çıkı:

1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
......