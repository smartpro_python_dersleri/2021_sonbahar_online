"""
"import" python içerisinde başka bir dosya/kütüphane/modül içerisinden bir içeriği
(değişken, fonksiyon, class, obje vb..) o esnada çalıştığımız dosya içerisine çekmek
için kullandığımız bir komuttur.

Bu komut ile herhangi bir kütüphane ya da başaka bir dosyadan istediğimiz objeyi çekebiliriz.

Önemli not: burada örnek olsun diye hem birden fazla tekrar, hem de ayrık bir biçimde import yaptık.
Ancak gerçek bir kullanımda bu importların hepsi dosyanın en başında olacak şekilde yapılır.
"""

# Burada Python'in random kütüphanesini çektik.
import random
# random kütüphanesi içerisindeki "randint" fonksiyonu ile 1 ile 100 arasında rastgele bir yazı belirliyoruz.
print(random.randint(1, 100))

"""
"import random" dediğimizde bütün random kütüphanesini çekmiş olduk
Bu yöntem ram kullanımı için kötü bir yöntemdir. Onun yerine sadece kullanacağımız içeriği çekmemiz lazımdır.
Bunu yapmak için "from" komutu kullanılır
"""
from random import randint
# from ile çağırdığımız için sadece randint çağırılmış oldu. O yüzden direkt "randint" olarak kullanabiliyoruz.
print(randint(100, 1000))

# Bir modülden birden fazla içerik çekmek istersek, bunları virgül ile ayırıyoruz.
from random import randint, choice
print(randint(1000, 2000))
print(choice(['Ahmet', 'Mehmet', 'Veli', 'Mustafa', 'Mahmut']))

# Burada oluşturduğumuz "ornek_kutuphane.py" dosyası içerisindeki "get_random_choice" fonksiyonunu çağırdık
from ornek_kutuphane import get_random_choice

get_random_choice(['elma', 'armut', 'nar', 'ananas', 'muz', 'pastirma'], 3)

# aynı dosya içerisindeki "say_hello_to" fonksiyonu kendisini çağırmadığımız için burada kullanmaya kalkışırsak hata verecektir.
# say_hello_to('Göktuğ')

# Başka bir klasör içerisindeki bir dosyayı kullanmak için aralarında nokta ile ayrım yapılır.
from ornek_modul.ornek_dosya import count_character
count_character('Merhaba zalim dünya', 'a')

"""
Çağırdığımız objenin başka bir şey ile çakışmaması ya da daha kolay kullanılabilecek bir isimde olmasını
istersek eğer import sonrası "as" ile import ettiğimiz şeye başka bir isim verebiliyoruz.
"""
# Bu örnekte çektiğimiz randint fonksiyonu, hali hazırda olan randint'i ezmek yerine "rint" adı ile çağırılacaktır.
from ornek_modul.ornek_dosya import randint as rint
randint(1, 4)
rint()