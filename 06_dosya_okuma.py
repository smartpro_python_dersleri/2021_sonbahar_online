"""
Dosya okuma-yazma işlemlerinde "open" komutu kullanılır. Bu yolunu belirttiğiniz dosya üzerinde işlem yapmanızı sağlar.
İlk argüman olarak dosyanın konumunu, ikinci argüman olarak da dosyanın ne şekilde açılacağını belirler
Açabileceğiniz modlar:
- "r": Read modudur, yani dosyayı sadece okumak için açmanızı sağlar.
- "w": Write modudur. yani dosyaya yazmanızı sağlar. Bu mod dosyanın en başından itibaren yazar.
- "x": Var olmayan bir dosyayı yaratmak için kullanılır. Not: "w" modu da dosya yoksa eğer sıfırdan yaratır.
- "a": Var olan bir dosyanın sonundan itibaren ekleme yapmak için kullanılır.
Daha fazla bilgi için:
https://docs.python.org/3/library/functions.html#open
"""

# open komutu ile dosyamizi actik
dosya = open('06.1_dosya_okuma_ornek.md', 'r')

# dosya icerisindeki read komutu ile dosyanin icerigini okuduk
print(dosya.read())

# close komutu ile dosyayi okumayi biraktik, yani programimiz artik bu dosya ile bir islem yapmiyor
dosya.close()

"""
with komutu open gibi bir açılma ve kapanma koşulu olan komutları daha organize olarak kullanmamızı sağlayan
bir komuttur. Çalışma şekli, içerisinde yazdığımız kod öbeği "open" ve "close" arasında çalışır, içerisindeki
kod grubu bittiğinde otomatik olarak close işlemini yapar. Bu sayede dışarısında dosya kapanmış olur.
"""
with open('06.1_dosya_okuma_ornek.md', 'r') as dosya:
    # readlines komutu dosyanın satırlarını bize bir liste olarak dönecektir.
    print(dosya.readlines())
# Kod buraya geldiğinde dosya çoktan kapanmış olacaktır.

# Dosya içerisine bir şeyler yazdırmak için open komutunun mode değerini "w" olarak değiştiriyoruz.
# ancak bu modda iken dosyaya her yazma işlemimiz, dosyanın bütün içeriğini ezecektir.
with open('06.2_dosya_yazma_ornek.md', 'w') as yazilacak_dosya:
    metin = 'Bir varmış bir yokmuş, bir zamanlar onlineda öğrencileri gelen bir python kursu varmış.'
    metin += '\nVe bu kursun kel ama iyi bir hocası varmış.'

    # açtığımız dosya objesinde "write" komutunu kullanarak istediğimiz bir metin objesini içerisine yazdırabiliriz.
    yazilacak_dosya.write(metin)

# Burada önceki yazdıklarımızı ezeceğiz. 06.2_dosya_yazma_ornek.md dosyasının içeriği değişecek
with open('06.2_dosya_yazma_ornek.md', 'w') as yazilacak_dosya:
    yazilacak_dosya.write('bu da herseyin uzerine yazacak')

# Dosyanın içeriğini değiştirmeden sonuna ekleme yapmak istersek de "a" modunu kullanıyoruz.
with open('06.3_dosya_sona_ekleme_ornek.md', 'a') as eklenecek_dosya:
    eklenecek_dosya.write('Bu bir deneme satiridir\n')
    eklenecek_dosya.write('Bu da bir baska satirdir\n')

# "a" modu dosyanın içeriği ne olursa olsun sonuna ekleme yapacaktır.
with open('06.3_dosya_sona_ekleme_ornek.md', 'a') as eklenecek_dosya:
    eklenecek_dosya.write('Bu ise bir katir degildir...')