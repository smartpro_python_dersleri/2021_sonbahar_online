class Ogrenci:
    def __init__(self, name, surname, age, classroom, gpa):
        self.name = name
        self.surname = surname
        self.age = age
        self.classroom = classroom
        self.gpa = gpa

    def __str__(self):
        # __str__ methodu oluşturduğumuz objeyi string haline getirirsek nasıl bir sonuç vermesi gerektiğini belirler
        return f'{self.classroom} - {self.get_full_name()}'
    
    @property
    def full_name(self):
        return self.get_full_name()

    def get_full_name(self):
        # Bu method bize objenin içerisindeki bilgileri kullanarak Öğrenci objesinin tam adını bize döner.
        return f'{self.name} {self.surname}'

    another_fullname = property(get_full_name)

    def change_gpa(self, gpa):
        # Bu method kendisine verdiğimiz argümanı kullanarak Öğrenci objesinin GPA verisini günceller.
        self.gpa = gpa
        print(f'Öğrencinin not ortalaması {gpa} olarak güncellenmiştir.')
    
    @classmethod
    def print_class_name(cls):
        print(cls.__name__)

    def _new_classmethod(cls):
        print(cls)

    new_classmethod = classmethod(_new_classmethod)

    @staticmethod
    def random_number():
        from random import randint
        return randint(0, 1000)

bg = Ogrenci('Batuhan', 'Günaydın', 27, 'SM_ONLINE_1', 90)

# Bu hata verir çünkü içerisinde bir obje mevcut değildir
# Ogrenci.get_full_name()

Ogrenci.print_class_name()
bg.print_class_name()

print(bg.get_full_name())
# Buradaki örnekte olduğu gibi class altında normal methodları çalıştırabiliriz.
# Ama bunun için kendisine objeyi argüman olarak vermemiz gereklidir.
print(Ogrenci.get_full_name(bg))

print(Ogrenci.random_number())
print(bg.random_number())

print(bg.gpa)
print(bg.full_name)
print(bg.another_fullname)

print(Ogrenci.new_classmethod())