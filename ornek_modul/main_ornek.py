text1 = 'Hello There'
text2 = 'General Kenobi?'

def main():
    print(f'{text1}\n{text2}')

if __name__ == '__main__':
    main()