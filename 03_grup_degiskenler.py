# Tuple, List, Set, Dict

# Tuple
"""
Tuplelar birden fazla değişkeni kendi içerisinde sıralı bir biçimde tutan bir grup değişkenidir.
Tuplelar sonradan modifiye edilemez.
"""
ornek_tuple = ('ali', 'veli', 'mehmet')
ornek_tuple_2 = 'huseyin', 'mahmut', 'berkay'

print(ornek_tuple)
print(ornek_tuple_2)

# Type komutu içerisine verdiğimiz değişkenin veri tipini bize döndürür
print(type(ornek_tuple), type(ornek_tuple_2))

# Tuple'ın ilk elemanını yani indexteki sıfırıncı elemanı döner
ornek_tuple[0]
# Tuple'ın ikinci elemanını döner
ornek_tuple[1]
# Tuple'ın son elemanını döner
ornek_tuple[-1]
# Tuple'ın indexi 2 olan yani üçüncü elemanına kadar olan kısmını döner
ornek_tuple[:2]
# Tuple'ın indexi 1 olan elemanı ve sonrasını döner
ornek_tuple[1:]
# Tuple'ın belirttiğimiz indexler arasınaki elemanlarını döner
ornek_tuple[1:2]

# List
"""
Listeler Tuplelara benzer, aralarındaki fark ise sonradan modifiye edilmeye açıklardır.
Listeler köşeli parantez içerisinde tanımlanır.
Listeden indexe göre eleman çekme tuple ile aynıdır
"""
ornek_liste = ['Ozan', 'Yargi', 'Ali', 'Celal']

print(ornek_liste)
print(ornek_liste[1])

# Listeye eleman ekleme
ornek_liste.append('Kubra')
print(ornek_liste)

# İndexten fazla bir eleman çağırmak isterseniz eğer out of range şeklinde hata alırsınız ve kodunuz çalışmaz
# print(ornek_liste[5])

# Pop komutu indexini belirttiğiniz ögeyi alır, kodda kullanmanız için size değişken olarak verir ve listeden kaldırır.
pop_ile_cikartilan = ornek_liste.pop(1)
print(pop_ile_cikartilan)
print(ornek_liste)

# Remove komutu spesifik bir elemanı listeden kaldırmamızı sağlar
ornek_liste.remove('Ali')
print(ornek_liste)

# Bu şekilde de indexini belirttiğimiz elemanı modifiye edebiliriz
ornek_liste[1] = 'Berkay'
print(ornek_liste)

ornek_liste.append('Ozan')

# Count komutu içerisinde belirttiğimiz ögenin liste içerisindeki sayısını döner
ornek_liste.count('Ozan')  # 2
ornek_liste.count('Kubra')  # 1
ornek_liste.count('Yargi')  # 0

# Count komutu stringlerde de geçerlidir ve stringin karakterlerini sayar
ornek_string = 'Bu örnek bir stringdir ve sadece count komutunu denemek için kullanılacaktır.'
ornek_string.count('e')  # 7


# Set
"""
Setler tıpkı Tuplelar gibi sonradan modifiye edilemeyen bir veri tipidir.
Ancak List ve Tupleların aksine veriyi sıralı şekilde tutmazlar.
Ayrıca setler içerisinde aynı veriden birden fazla da tutmaz
"""

ornek_set = {'ahmet', 'ali', 'zafer', 'ugur'}
print(ornek_set)

# set içerisinden index ile eleman çağırmaya çalışmak yani ornek_set[2] şeklinde çağırmak size sadece hata dönecektir!

"""
Setlerin sık kullanıldığı iki büyük alan var.
1- Büyük bir veri havuzunu daha az sistem tüketerek işlemek.
2- Bir liste içerisinden tekrarlanan elemanları çıkartmak için önce sete çevrilip sonra tekrar listeye çevrilmek
"""

# Dict - Sözlük
"""
Sözlükler bir anahtar kelime ve değer ikilisinden oluşan ögeler grubudur.
"""

ornek_dict = {
    'name': 'İnsan',
    'surname': 'Ademoğlu',
    'age': 25,
    'gender': 'male'
}

# Sözlüklerden eleman çağırmak için köşeli parantez içerisinde aradığımız anahtar kelimeyi yazıyoruz
print(ornek_dict['age'])
# Ek olarak get komutu ile de çağırılabilir. Olmayan bir öge varsa köşeli parantez hata verir, get boş veri döner
print(ornek_dict.get('gender'))

# bir sözlüğün bütün elemanlarını döner
print(ornek_dict.items())

# bir sözlüğün anahtar kelimenelerini bize döner
print(ornek_dict.keys())

# bir sözlüğün değerlerini liste olarak bize döner
print(ornek_dict.values())

# Bir sözlüğün herhangi bir elemanını değiştirmek için o elemanı çağırır gibi kullanıp ardından değer atayabilirsiniz.
ornek_dict['age'] = 30

# Bir sözlüğe yeni bir öge eklemek ya da var olan bir ögeyi düzenlemek için update komutu kullanılır.
# update komutu içerisine girilecek değişiklik gene bir sözlük olarak girilir.
ornek_dict.update({'job': 'İnsan Kaynakları'})

ikinci_ornek_dict = {
    'phone': '+905555555555',
    'email': 'insan_ademoglu@dunya.com.tr.co.uk'
}

# update komutunu kullanarak iki farklı sözlüğü birleştirebiliriz
ornek_dict.update(ikinci_ornek_dict)
print(ornek_dict)

# Eğer aynı anahtar kelimeye sahip iki öge eklersek, sözlük sadece en son yazdığımız hangisi ise onu kullanacaktır.
yeni_ornek_dict = {
    'name': 'insan',
    'surname': 'ademoglu',
    'name': 'adem'
}
print(yeni_ornek_dict)