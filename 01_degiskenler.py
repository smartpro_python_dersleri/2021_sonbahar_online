# str - String - Metin
"""
Buraya yazdığım hiçbir şey Python tarafından okunmayacak
Direkt atlanacak
"""
metin = "Bir deneme metni"
print(metin)

# int - Integer - Düz Sayı
duz_sayi = 42

# float - Float - Küsüratlı Sayı
kusurlu_sayi = 12.9889

# bool - Boolean - Doğru-Yanlış / True-False
dogru = True
yanlis = False

# burasi bize duz_sayi değişkenini 12 ile toplayıp terminale çıktı olarak verecek
print(
    duz_sayi + 12
)

# duz_sayi değişkenine 2 ekleyip tekrar duz_sayi olarak kaydettik
duz_sayi = duz_sayi + 2
print(duz_sayi)

# Yukarıdaki işlem ile birebir aynı sonucu vermektedir
duz_sayi += 2
print(duz_sayi)

duz_sayi -= 4
print(duz_sayi)

# bölme
print(duz_sayi / 2)
print(int(duz_sayi / 5))

# çarpma
print(duz_sayi * 2)

# Mod alma
print(duz_sayi%10)

# str değişkenine ekleme yapmak
print(metin + "." + " Ve çok da güzel bir deneme metnidir")

metin += ". Bu da sonradan eklemesi."
print(metin)

print(metin, "Bir başka metin")

print("Beykay'ın")
# tırnak ya da çift tırnak gibi ögeleri string içerisinde kullanmak için ters slash "\" kullanıp ardından özel karakteri yazabilirsiniz
# bu sayede python orayı bir kod parçası olarak değil, yazının bir parçası olarak görür ve özel bir işlem yapmaz.
print('Beykay\'ın')
print("Ve şair \"Hacı beni neden şair yaptınız, bende yok öyle bir yetenek\" dedi!")
print("Biz \"\\\" karakterini yazmak istedik, hem de tırnak içinde")