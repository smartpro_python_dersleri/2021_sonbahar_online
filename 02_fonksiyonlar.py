# Fonksiyonlar "def" anahtar kelimesi ile tanımlanır
def ilk_fonksiyon():
    print("ilk fonksiyonumuzu yarattık")
    print("Sonra da ikinci bir kod satırı daha ekledik")
print("Burasi fonksiyon disinda yer almaktadir")

# Fonksiyonu çalıştırmak için kodda çağıracağımız yerde adını yazıp sonuna parantez koyuyoruz
ilk_fonksiyon()
ilk_fonksiyon()

# Argüman alacak fonksiyonlarda argüman parantez içerisinde belirtilir
def carpi_dort(sayi):
    print(sayi * 4)

carpi_dort(8)
carpi_dort(3)

# Birden fazla argüman tanımlamada virgül kullanırız
def toplama(sayi_1, sayi_2):
    print(sayi_1 + sayi_2)

toplama(12, 30)

# Öntanımlı değerli argüman
def bolme(bolunen=10, bolen=2):
    """
    burasi bir docstring
    """
    print(bolunen/bolen)

bolme()
bolme(bolen=5, bolunen=100)

# öntanımlı değeri olan argümanlar olmayanlardan sonra yazılmalıdır
def yazdir(yazi, noktalama="."):
    print(yazi + noktalama)

yazdir("Deneme yazisi")
yazdir("BU DA BAĞIRMA YAZISI", noktalama='!!!')

# iç içe fonksiyonlar
def ana_fonksiyon():
    def buyut(metin):
        print(metin.upper())
    buyut("Merhaba Dünya")

ana_fonksiyon()
# buyut fonksiyonunu ana_fonksiyon dışında çağıramazsınız
# buyut()

# return fonksiyonun programımız tarafından okunabilecek bir veri dönmesini sağlar
def birler_basamagi(sayi):
    return sayi%10
    print('deneme')

fonksiyon_sonucu = birler_basamagi(42)

print(fonksiyon_sonucu)