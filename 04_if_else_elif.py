## Operatorler

# Eşittir - "==" şeklinde yazılır, verilen iki değer eşitse True, değilse False döner
"ahmet" == "ahmet"  # True
12 == 12  # True
"Mustafa" == "Berkay"  # False

# Eşit Değildir - "!=" şeklinde yazılır, verilen iki değer eşit değilse True, eşitse False döner
"ahmet" != "ahmet"  # False
12 != 12  # False
12 != 18  # True
"Mustafa" != "Berkay"  # True

# Büyüktür - ">" şeklinde yazılır, verilen ilk değer ikinci değerden bütükse True, değilse False döner
12 > 12  # False
12 > 20  # False
12 > 10  # True

# Küçüktür - "<" şeklinde yazılır, verilen ilk değer ikincisinden küçükse True, değilse False döner
12 < 12  # False
12 < 20  # True
12 < 10  # False

# Büyük Eşittir - ">=" şeklinde yazılır, verilen ilk değer ikincisinden büyük ya da eşitse True, değilse False döner
12 >= 12  # True
12 >= 20  # False
12 >= 10  # True

# Küçük Eşittir - "<=" şeklinde yazılır, verilen ilk değer ikincisinden küçük ya da eşitse True, değilse False döner
12 <= 12  # True
12 <= 20  # True
12 <= 10  # False

# İçerir - "in" şeklinde yazılır, verilen ilk değerin ikincisinin içinde olup olmadığını kontrol eder. Genelde liste ya da sözlüklerde kullanılır
'ahmet' in ['ali', 'veli', 'huseyin', 'ahmet']  # True
'mehmet' in ['ali', 'veli', 'huseyin', 'ahmet']  # False

# Is - "is" şeklinde yazılır, verilen iki değerin birebir aynı veri olması koşulunda True döner. Burada aynı değer olması da yetersizdir.
a = 12
a is a  # True

# Değildir - "not" şelinde yazılır, çıkan sonucu tersine çevirir
not True  # False

## Kontroller

# if içerisine verdiğimiz operatör, yani denetlenecek şey True olursa eğer, if içerisindeki kod bloğu çalışır
if True:
    print('gecti')

if False:
    print("bu da geçmedi")

if 12 == 12:
    print('12 ile 12 esittir')

# Else - if sorgusu içerisinde denetlediğimiz koşulun tutmaması durumunda çalışacak kod parçacığını belirtmemizi sağlar

if 'ali' == 'ali':
    print('Burada else yok')
else:
    print('burasi calismadi iste')

if 'ali' == 'yargi':
    print('bu gelmeyecek')
else:
    print('Bu sefer else calisti')

# Elif - if sorgusu içerisindeki koşulumuz tutmadığı durumda başka bir koşul daha denetlemek istersek kullanacağımız komuttur
if 'ali' == 'veli':
    print('buraya gelmez')
elif 'ali' == 'mehmet':
    print('buraya gelmez')
elif 'ali' == 'ali':
    print('Elif komutumuz calisti')
else:
    print('buraya gelmez')

# HATALI
"""
if 'ali' == 'veli':
    print('buraya gelmez')
if 'ali' == 'mehmet':
    print('buraya gelmez')
if 'ali' == 'ali':
    print('Elif komutumuz calisti')
else:
    print('buraya gelmez')
"""

## Grup operatorler

# Soru- a, b'ye eşit, c'den büyüktür şeklinde bir sorgu nasıl yapılır?
a = 12
b = 12
c = 10
# YANLIŞ cevap:
if a == b:
    if a > c:
        print('sonuç doğru ama yöntem yanlış')
    
# DOĞRU cevap:
if a == b and a > c:
    print('Bu ise doğru yöntemle elde edilmiş doğru cevap')

# VE - "and" şeklinde yazılır, iki sorgunun da doğru olması gereklidir
True and False  # False
False and True  # False
False and False  # False
True and True  # True

# VEYA - "or" şeklinde yazılır, iki sorgudan birinin doğru olması gereklidir
True or False  # True
False or True  # True
False or False  # False
True or True  # True